#!/usr/bin/env python
# -*- coding: utf-8 -*-
###############################################################################
# @copyright Copyright (C) Guichet Entreprises - All Rights Reserved
#     All Rights Reserved.
#     Unauthorized copying of this file, via any medium is strictly prohibited
#     Dissemination of this information or reproduction of this material
#     is strictly forbidden unless prior written permission is obtained
#     from Guichet Entreprises.
###############################################################################

###############################################################################
# Complete po file with google translation
#
###############################################################################

import logging
import sys
import os
import polib

import pymdtools.common as common
from pymdtools.translate import translate_txt as translate_txt
from pymdtools.translate import eu_lang_list as eu_lang_list


###############################################################################
# Fill all the missing part in the po file with automated translation
#
# @param locale_folder the locale folder
# @param ref_lang the reference language
# @param lang the langugae to work with
# @return the po.
###############################################################################
def fill_po(locale_folder, ref_lang, lang, module_name):
    logging.info('fill %s with ref lang %s', lang, ref_lang)
    if ref_lang == lang:
        return

    markup_stamp = "--Machine translation--"
    po_ref_filename = os.path.join(locale_folder, ref_lang,
                                   "LC_MESSAGES", module_name + '.po')
    po_lang_filename = os.path.join(locale_folder, lang,
                                    "LC_MESSAGES", module_name + '.po')

    po_ref = polib.pofile(po_ref_filename)
    po_lang = polib.pofile(po_lang_filename)

    logging.info('Reference language (%s) has %d message untranslated.',
                 ref_lang, len(po_ref.untranslated_entries()))

    logging.info('The language (%s) has %d message untranslated.',
                 lang, len(po_lang.untranslated_entries()))

    counter = 0
    counter_un = len(po_lang.untranslated_entries())

    for entrie in po_lang:
        ref_entries = [ent for ent in po_ref.translated_entries()
                       if ent.msgid == entrie.msgid]
        if len(ref_entries) == 0:
            logging.info('The msgid "%s" has no translation in language %s',
                         entrie.msgid, ref_lang)
            continue
        if len(ref_entries) > 1:
            logging.info('HUGE PROBLEM: msgid "%s" has many '
                         'translation in language %s', entrie.msgid, ref_lang)
            continue

        if entrie.translated() and markup_stamp not in entrie.comment:
            continue

        if entrie.translated():
            logging.info('Rework the msgid "%s"', entrie.msgid)

        # Call the machine transaltion
        entrie.msgstr = translate_txt(ref_entries[0].msgstr,
                                      src=ref_lang, dest=lang)
        if markup_stamp not in entrie.comment:
            entrie.comment = entrie.comment + "\n" + markup_stamp + "\n"

        counter += 1

        logging.info("%03d/%03d: %70s", counter, counter_un,
                     ref_entries[0].msgstr)

    if counter == 0:
        return

    common.create_backup(po_lang_filename)
    po_lang.save()

###############################################################################
# Get the local folder of this script
#
# @return the local folder.
###############################################################################
def get_local_folder():
    return os.path.split(os.path.abspath(os.path.realpath(
        __get_this_filename())))[0]

###############################################################################
# Find the filename of this file (depend on the frozen or not)
# This function return the filename of this script.
# The function is complex for the frozen system
#
# @return the filename of THIS script.
###############################################################################
def __get_this_filename():
    result = ""

    if getattr(sys, 'frozen', False):
        # frozen
        result = sys.executable
    else:
        # unfrozen
        result = __file__

    return result


###############################################################################
# Set up the logging system
###############################################################################
def __set_logging_system():
    log_filename = os.path.splitext(os.path.abspath(
        os.path.realpath(__get_this_filename())))[0] + '.log'
    logging.basicConfig(filename=log_filename, level=logging.DEBUG,
                        format='%(asctime)s: %(message)s',
                        datefmt='%m/%d/%Y %I:%M:%S %p')
    console = logging.StreamHandler()
    console.setLevel(logging.INFO)
    # set a format which is simpler for console use
    formatter = logging.Formatter('%(asctime)s: %(levelname)-8s %(message)s')
    # tell the handler to use this format
    console.setFormatter(formatter)
    # add the handler to the root logger
    logging.getLogger('').addHandler(console)


###############################################################################
# Main script call only if this script is runned directly
###############################################################################
def __main():
    # ------------------------------------
    logging.info('Started %s', __get_this_filename())
    logging.info('The Python version is %s.%s.%s',
                 sys.version_info[0], sys.version_info[1], sys.version_info[2])

    logging.info('Fill all the po.')
    folder_locale = os.path.join(get_local_folder(),
                                 "..", "src", "locale")
    logging.info('Locale folder: %s', folder_locale)

    ref_lang = "fr"

    for lang in eu_lang_list():
        fill_po(folder_locale, ref_lang, lang, "xenon")

    # fill_po(folder_locale, ref_lang, "bg", "xenon")

    logging.info('Finished')
    # ------------------------------------


###############################################################################
# Call main function if the script is main
# Exec only if this script is runned directly
###############################################################################
if __name__ == '__main__':
    __set_logging_system()
    __main()
