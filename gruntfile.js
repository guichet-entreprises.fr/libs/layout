// ----------------------------------------------------------------------------
// Copyright (c) 2019 Guichet Entreprises
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//
// ----------------------------------------------------------------------------

'use strict';

const path = require('path');
const util = require('util');
const utils = require('./build/utils');
const DependencyLoader = require('./build/dependencies');

const project = {
    build: {
        directory: 'dist',
        outputDirectory: 'dist',
        releaseDirectory: 'release',
        sourceDirectory: 'src'
    }
};



/*
 * Dependencies overriding
 */
const overrides = require('./build/overrides.json');



module.exports = function (grunt) {

    const assetsWorkingDir = '_tmp/assets';

    const pkg = grunt.file.readJSON('package.json');

    let scripts = grunt.file.expandMapping(['**/*.js'], '<%= project.build.outputDirectory %>/js', {
        cwd: project.build.sourceDirectory + '/js'
    });

    let vendorsStyles = Object.keys(pkg.dependencies || {}) //
        .map(name => {
            let basePath = 'node_modules/' + name;
            let nfo = grunt.file.readJSON(basePath + '/package.json');
            let styles = (nfo.less || nfo.style);

            if (null == styles) {
                return null;
            } else if (Array.isArray(styles)) {
                return styles.map(function (elm) {
                    return utils.buildStyle(basePath, elm);
                });
            } else {
                return [utils.buildStyle(basePath, styles)];
            }
        }) //
        .filter(lst => null != lst && lst.length > 0) //
        .reduce((all, lst) => all.concat(lst), []) //
        .concat(grunt.file.expandMapping(['css/libs/**/*.less'], null, {
            cwd: project.build.sourceDirectory + '/assets'
        }).map(function (elm) {
            console.log('*** ', elm)
            return {
                src: elm.src,
                dest: assetsWorkingDir + '/css/' + elm.dest.replace(/\.[^.]+$/, '.css')
            };
        }));


    let deps = DependencyLoader.build(grunt);

    const staticBasePath = grunt.option('static') ? 'assets/guichet-entreprises' : grunt.option('release') ? 'https://statics.guichet-entreprises.fr' : 'https://localhost:1443';
    const feedBackDomain = 'https://feedback-ws.dev.guichet-entreprises.fr/';


    const messages = grunt.file.readJSON('messages_fr.json');

    var templateDefaultContext = {
        angular: false,
        applyTheme: false,
        basePath: '',
        externalConfig: true,
        feedBackDomain: feedBackDomain,
        mainMenu: 'menu.html',
        mainContent: 'layout-main-content.html',
        release: true === grunt.option('release'),
        scripts: true === grunt.option('release') ? ['/js/vendors.min.js', '/js/libs.min.js'] : ['/js/vendors.js', '/js/libs.js'],
        staticBasePath: staticBasePath,
        styles: vendorsStyles.map(elm => '/' + elm.dest.replace(/^.*\/(css\/)/, '$1').replace(/\.([^.]+)$/, '.min.$1')).reduce((dst, elm) => dst.indexOf(elm) < 0 ? dst.concat([elm]) : dst, []),
        subTheme: 'default',
        thymeleaf: false,
        links: {
            account: {
                login: 'https://account.guichet-entreprises.fr',
                logout: 'https://account.guichet-entreprises.fr/disconnect',
                profil: 'https://account.guichet-entreprises.fr/users/modify',
                register: 'https://account.guichet-entreprises.fr/users/new',
            }
        },
        version: pkg.version,
        i18n: (terms) => terms,
        theming: (attr, lnk) => `th:${attr}="@{|\${@appProperties.staticBasePath}${encodeURI(lnk)}|}" ${attr}="${encodeURI(staticBasePath)}${encodeURI(lnk)}"`,
        link: (lnk) => `href="http://www.guichet-entreprises.fr${encodeURI(lnk)}"`
    };

    
    var templateDefaultReplacements = [
        {
            pattern: /<([a-z0-9:-]+)((?:\s+[a-z0-9:-]+="[^"]*")*)(?:\s+th:u?text="#\{([^"]+)\}")((?:\s+[a-z0-9:-]+="[^"]*")*)(\/>|>([^<]*)<\/\1>)?/gi,
            replacement: function (path, src, tagName, attrsBefore, text, attrsAfter, closer, innerText) {
                return '<' + tagName + attrsBefore + attrsAfter + '>' + (!innerText || '' == innerText.trim() ? utils.htmlDecode(text) : innerText) + '</' + tagName + '>';
            }
        },
        {
            pattern: / xmlns:th="[^"]+"/gm,
            replacement: ''
        },
        {
            pattern: / th:([a-z]+)="([^"]+)"/gmi,
            replacement: ''
        },
        {
            pattern: /<th:block\s*(?:\/>|>(.*)<\/th:block>)/gi,
            replacement: function (path, src, text) {
                return text || '';
            }
        }
    ];

    const themes = ['guichet-entreprises', 'guichet-partenaires', 'guichet-qualifications'];

    const CSP = {
        'default-src': ['none'],
        'style-src': ['self', feedBackDomain, 'https://localhost:1443'],
        'object-src': ['self'],
        'script-src': ['self', feedBackDomain, 'https://localhost:1443'],
        'form-action': ['self'],
        'base-uri': ['self'],
        'connect-src': ['self', feedBackDomain],
        'img-src': ['self', feedBackDomain, 'https://localhost:1443'],
        'font-src': ['self', 'https://localhost:1443'],
        'frame-ancestors': ['none']
    };

    const httpsConnectMiddleware = function (req, res, next) {
        res.setHeader('Content-Security-Policy', Object.keys(CSP).map(key => key + ' ' + CSP[key].map(value => (/^(self|unsafe-(inline|eval|hashes)|none|(nonce|sha256|sha384|sha512)-.*|report-sample)/i).test(value) ? "'" + value + "'" : value).join(' ')).join('; '));
        res.setHeader('Strict-Transport-Security', 'max-age=63072000; includeSubDomains; preload');
        res.setHeader('Referrer-Policy', 'same-origin');
        res.setHeader('X-Content-Type-Options', 'nosniff');
        res.setHeader('X-Frame-Options', 'SAMEORIGIN')
        res.setHeader('X-XSS-Protection', '1; mode=block');
        res.setHeader('Access-Control-Allow-Origin', '*');
        res.setHeader('Access-Control-Allow-Headers', 'x-ge-domain');

        next();
    };

    const domainConnectMiddleware = function (req, res, next) {
        let prefix = '/guichet-entreprises';
        let domain = req.headers['x-ge-domain'];
        if ('guichet-partenaires' == domain || 'guichet-qualifications' == domain) {
            prefix = '/' + domain;
        }

        req.url = prefix + req.url;

        next();
    };

    /*
     * Command line to generate self-signed certificate :
     *     openssl req -x509 -nodes -days $((5*365)) -newkey rsa:4096 -subj '/C=FR/O=SCNGE/CN=layout.local' -keyout local.key -out local.crt
     */
    const defaultSslConfiguration = {
        protocol: 'https',
        port: 2443,
        key: grunt.file.read('cert/local.key').toString(),
        cert: grunt.file.read('cert/local.crt').toString(),
        // ca: grunt.file.read('cert/ca.crt').toString(),
        base: 'dist',
        middleware: function (connect, options, middlewares) {
            middlewares.unshift(httpsConnectMiddleware);

            return middlewares;
        }
    };



    grunt.initConfig({
        pkg: pkg,
        project: project,
        requirejs: {
            options: {
                nodeRequire: require,
                baseUrl: '.',
                optimize: grunt.option('release') ? 'uglify' : 'none',
                onBuildRead: function (moduleName, path, contents) {
                    return deps.encapsulate(moduleName, contents);
                }
            },
            vendors: {
                options: {
                    generateSourceMaps: false,
                    preserveLicenseComments: false,
                    uglify2: {
                        comments: function () { console.log(arguments); return false; }
                    },
                    include: ['requireLib'].concat(Object.keys(deps.vendors)),
                    paths: utils.extend(deps.vendors, { 'requireLib': 'node_modules/requirejs/require' }),
                    out: assetsWorkingDir + '/js/vendors' + (grunt.option('release') ? '.min' : '') + '.js'
                }
            },
            application: {
                options: {
                    name: 'main',
                    paths: deps.empties,
                    out: assetsWorkingDir + '/js/libs' + (grunt.option('release') ? '.min' : '') + '.js',
                    baseUrl: 'src/assets/js/amd'
                },
                files: [
                    {
                        expand: true,
                        src: ['**/*.js', '!main.js'],
                        cwd: 'src/assets/js/amd',
                        ext: '',
                        extDot: 'last'
                    }
                ]
            }
        },
        less: {
            vendors: {
                files: vendorsStyles
            },
            app: {
                options: {
                    paths: ['<%= project.build.sourceDirectory %>/styles/mixins', 'node_modules/bootstrap/less', 'node_modules/font-awesome/less'],
                    modifyVars: {
                        staticBasePath: staticBasePath
                    }
                },
                files: [
                    {
                        expand: true,
                        filter: 'isFile',
                        src: ['**/*.less', '**/*.css', '!**/libs/**', '!**/mixins/**', '!**/mixins2/**'],
                        cwd: project.build.sourceDirectory + '/assets',
                        dest: '<%= project.build.outputDirectory %>/assets',
                        ext: '.css',
                        extDot: 'last'
                    }
                ]
            }
        },
        cssmin: {
            options: {
                level: {
                    1: {
                        all: true,
                        roundingPrecision: false,
                        selectorsSortingMethod: 'standard',
                        specialComments: 0
                    }
                },
                sourceMap: !grunt.option('release'),
                sourceMapInlineSources: !grunt.option('release')
            },
            dev: {
                files: [
                    {
                        expand: true,
                        src: ['**/*.css', '!**/*.min.css'],
                        cwd: project.build.outputDirectory + '/assets',
                        dest: project.build.outputDirectory + '/assets',
                        ext: '.min.css',
                        extDot: 'last'
                    }
                ]
            }
        },
        extract: {
            vendors: {
                files: vendorsStyles
            },
            app: {
                files: [
                    {
                        expand: true,
                        src: ['**/*.less', '**/*.css', '!**/libs/**', '!**/mixins/**', '!**/mixins2/**'],
                        cwd: project.build.sourceDirectory + '/assets',
                        dest: '<%= project.build.outputDirectory %>/assets',
                        ext: '.css',
                        extDot: 'last'
                    }
                ]
            }
        },
        copy: {
            commons: {
                files: themes.map(function (theme) {
                    return {
                        expand: true,
                        filter: 'isFile',
                        src: ['**/*.*'],
                        cwd: assetsWorkingDir,
                        dest: '<%= project.build.outputDirectory %>/assets/' + theme
                    };
                }).concat(
                    themes.map((theme) => {
                        return {
                            expand: true,
                            filter: 'isFile',
                            src: ['**/*.*', '!**/*.css', '!**/*.less', '!mixins/**/*.*'],
                            cwd: project.build.sourceDirectory + '/styles',
                            dest: '<%= project.build.outputDirectory %>/assets/' + theme + '/css'
                        }
                    })
                )
            },
            assets: {
                files: [
                    {
                        expand: true,
                        filter: 'isFile',
                        src: ['**/*/*', '!**/*.js', '!**/*.css', '!**/*.less'],
                        cwd: project.build.sourceDirectory + '/assets',
                        dest: '<%= project.build.outputDirectory %>/assets'
                    }
                ]
            },
            scripts: {
                files: themes.map(theme => {
                    return {
                        expand: true,
                        src: ['**/*.js', '!amd/**/*'],
                        cwd: project.build.sourceDirectory + '/assets/js',
                        dest: `<%= project.build.outputDirectory %>/assets/${theme}/js`
                    };
                })
            },
            binaries: {
                files: themes.map(function (theme) {
                    return {
                        expand: true,
                        filter: 'isFile',
                        src: ['jquery-ui.css', 'images/**'],
                        cwd: 'node_modules/jquery-ui/themes/base/',
                        dest: '<%= project.build.outputDirectory %>/assets/' + theme + '/css/libs/jquery-ui'
                    };
                }).concat([
                    {
                        expand: true,
                        filter: 'isFile',
                        src: ['**/*.*', '!**/*.html'],
                        cwd: project.build.sourceDirectory + '/html',
                        dest: '<%= project.build.outputDirectory %>'
                    }
                ])
            },
            flags: {
                files: themes.map(function (theme) {
                    return {
                        expand: true,
                        filter: 'isFile',
                        src: ['**/*.png'],
                        cwd: '_tmp/flags',
                        dest: '<%= project.build.outputDirectory %>/assets/' + theme + '/images'
                    };
                })
            }
        },
        watch: {
            options: {
                interval: 2000,
                spawn: false
            },
            styles: {
                files: ['assets/**/*.css', 'assets/**/*.less', 'styles/**/*.*'],
                tasks: ['less', 'extract', 'cssmin'],
                options: {
                    cwd: project.build.sourceDirectory
                }
            },
            scripts: {
                files: ['assets/**/*.js'],
                tasks: ['requirejs:application', 'copy:commons', 'copy:scripts'],
                options: {
                    cwd: project.build.sourceDirectory
                }
            },
            html: {
                files: ['html/**/*.html'],
                tasks: ['template'],
                options: {
                    cwd: project.build.sourceDirectory
                }
            },
            others: {
                files: [
                    '**/*.*',
                    '!**/*.less',
                    '!**/*.css',
                    '!**/*.js',
                    '!**/*.html'
                ],
                tasks: ['copy'],
                options: {
                    cwd: project.build.sourceDirectory,
                }
            }
        },
        connect: {
            server: {
                options: {
                    protocol: 'http',
                    port: 9001,
                    base: 'dist',
                    middleware: function (connect, options, middlewares) {
                        middlewares.unshift(
                            function (req, res, next) {
                                res.writeHead(301, { 'Location': 'https://' + req.headers.host.replace(/:[0-9]+$/, '') });
                                res.end();
                            }
                        );

                        return middlewares;
                    }
                }
            },
            ssl: {
                options: utils.extend(defaultSslConfiguration, {
                    port: 2443,
                    base: 'dist'
                })
            },
            resources: {
                options: utils.extend(defaultSslConfiguration, {
                    port: 1443,
                    base: 'dist/assets',
                    middleware: function (connect, options, middlewares) {
                        middlewares.unshift(httpsConnectMiddleware);
                        middlewares.unshift(domainConnectMiddleware);

                        return middlewares;
                    }
                })
            }
        },
        template: {
            options: {
                context: templateDefaultContext
            },
            nash: {
                options: {
                    context: utils.extend(templateDefaultContext, {
                        applyTheme: false,
                        basePath: '/theme/base-v3/',
                        staticBasePath: '/theme/base-v3/${subTheme}',
                        externalConfig: true,
                        messages: {
                            baseline: {
                                ge: 'Une seule adresse pour créer votre entreprise',
                                gq: 'Une seule adresse pour la reconnaissance de vos qualifications professionnelles',
                                gp: 'Une seule adresse pour les partenaires du service Guichet Entreprises'
                            }
                        },
                        subTheme: '${subTheme}',
                        thymeleaf: true,
                        theming: (attr, lnk) => `th:${attr}="@{|\${@appProperties.staticBasePath}${encodeURI(lnk)}|}" ${attr}="/theme/base-v3/\${subTheme}${encodeURI(lnk)}"`,
                        link: (lnk) => `th:href="@{\${@appProperties.frontUrl} + '${lnk}'}" href="http://www.guichet-entreprises.fr${encodeURI(lnk)}"`
                    }),
                    replacements: [
                        {
                            pattern: /(<link .*rel="stylesheet".*|<script .*|object .*|img .*|a .*) ?(href|src|data)="([^\"]+)"/gm,
                            replacement: function (path, src, prefix, attrName, attrValue) {
                                if (src.indexOf('th:' + attrName) > 0 || /^(#|https?:\/\/)/.test(attrValue)) {
                                    return src;
                                } else if (attrValue.indexOf('${') < 0) {
                                    return prefix.trim() + ' th:' + attrName + '="@{' + attrValue + '}" ' + attrName + '="' + attrValue + '"';
                                } else {
                                    return prefix.trim() + ' th:' + attrName + '="@{|' + attrValue + '|}" ' + attrName + '="' + attrValue + '"';
                                }
                            }
                        },
                        {
                            pattern: /<([a-z0-9]+)((?:\s+[a-z0-9:-]+="[^"]+")*)\s*>([^<]*)<\/\1>/gi,
                            replacement: function (path, src, tagName, attrs, text) {
                                if (src.indexOf('th:text') > 0 || '' === text.trim() || '&nbsp;' === text.trim()) {
                                    return src;
                                } else if ('script' == tagName) {
                                    return src;
                                } else {
                                    return '<' + tagName + ' ' + attrs.trim() + ' th:text="#{' + text + '}">' + text + '</' + tagName + '>';
                                }
                            }
                        },
                        {
                            pattern: /<([a-z0-9:-]+)([^>]*)(?:\s+(th:u?text)="#\{([^"]+)\}")([^>]*)(\/?)>/gi,
                            replacement: function (path, src, tagName, attrsBefore, textTag, text, attrsAfter, closer) {
                                return '<' + tagName + attrsBefore + ' ' + textTag + '="#{' + utils.htmlDecode(text) + '}"' + attrsAfter + closer + '>';
                            }
                        },
                        {
                            pattern: /\s+data-theme="[^"]+"/gi,
                            replacement: ''
                        }
                    ]
                },
                files: grunt.file.expandMapping(['nash/**/*.html'], '<%= project.build.outputDirectory %>', {
                    cwd: project.build.sourceDirectory + '/html'
                })
            },
            angular: {
                options: {
                    context: utils.extend(templateDefaultContext, {
                        applyTheme: false,
                        basePath: 'assets/',
                        staticBasePath: '',
                        externalConfig: true,
                        links: {
                            account: {
                                login: '{{login}}',
                                logout: '{{logout}}',
                                profil: '{{profil}}',
                                register: '{{createUser}}'
                            }
                        },
                        messages: {
                            baseline: {
                                ge: 'Une seule adresse pour créer votre entreprise',
                                gq: 'Une seule adresse pour la reconnaissance de vos qualifications professionnelles',
                                gp: 'Une seule adresse pour les partenaires du service Guichet Entreprises'
                            }
                        },
                        i18n: (terms) => messages[terms] || terms,
                        theming: (attr, lnk) => `[${attr}]="'${encodeURI(lnk.replace(/^\//, ''))}' | theming"`,
                        link: (lnk) => `href="{{www}}${encodeURI(lnk)}"`
                    }),
                    replacements: templateDefaultReplacements
                },
                files: grunt.file.expandMapping(['angular/**/*.html'], '<%= project.build.outputDirectory %>', {
                    cwd: project.build.sourceDirectory + '/html'
                })
            },
            others: {
                options: {
                    context: utils.extend(templateDefaultContext, {
                        messages: {
                            baseline: {
                                ge: 'La vie c\'est comme une boite de chocolat ...',
                                gq: 'La vie c\'est comme une boite de chocolat ...',
                                gp: 'La vie c\'est comme une boite de chocolat ...'
                            }
                        }
                    }),
                    replacements: templateDefaultReplacements
                },
                files: grunt.file.expandMapping(['**/*.html', '!nash/**/*.*', '!angular/**/*.*', '!fragments/**/*.*'], '<%= project.build.outputDirectory %>', {
                    cwd: project.build.sourceDirectory + '/html'
                }).concat(grunt.file.expandMapping(['js/config.js'], '<%= project.build.outputDirectory %>', {
                    cwd: project.build.sourceDirectory + '/assets'
                }))
            }
        },
        prepublish: {
            all: {
                options: {
                    pkg: {
                        name: pkg.name + '-{name}',
                        version: pkg.version,
                        publishConfig: pkg.publishConfig,
                        peerDependencies: pkg.dependencies
                    }
                },
                files: {
                    'assets': 'dist/assets',
                    'nash': 'dist/nash',
                    'angular': 'dist/angular'
                },
                // target: 'release'
            }
        },
        clean: {
            tmp: ['_tmp', 'tmp-*'],
            dist: [project.build.directory, project.build.outputDirectory, project.build.releaseDirectory]
        },
        targz: {
            assets: {
                files: [
                    {
                        expand: true,
                        cwd: 'dist/assets',
                        src: ['**/*'],
                        dest: ''
                    }
                ],
                target: '<%= project.build.releaseDirectory %>/layout-statics.tar.gz'
            },
            'assets-npm': {
                files: [
                    {
                        expand: true,
                        cwd: 'dist/assets',
                        src: ['**/*'],
                        dest: ''
                    },
                    {
                        src: ['<%= project.build.releaseDirectory %>/package-assets.json'],
                        dest: 'package/package.json'
                    }
                ],
                target: '<%= project.build.releaseDirectory %>/layout-assets.tar.gz'
            },
            nash: {
                files: [
                    {
                        expand: true,
                        cwd: 'dist/nash',
                        src: ['**/*'],
                        dest: 'package'
                    },
                    {
                        src: ['<%= project.build.releaseDirectory %>/package-nash.json'],
                        dest: 'package/package.json'
                    }
                ],
                target: '<%= project.build.releaseDirectory %>/layout-nash.tar.gz'
            }
        },
        maven: {
            options: {
                groupId: 'fr.ge.common',
                artifactId: 'layout',
                version: pkg.version.replace(/(^|[^0-9])0+/g, "$1"),
                generatePom: true,
                type: 'tar.gz',
                repositories: [
                    {
                        id: 'scnge',
                        url: 'https://tools.projet-ge.fr/nexus/content/groups/global/'
                    }
                ]
            },
            all: {
                files: {
                    'nash': '<%= project.build.releaseDirectory %>/layout-nash.tar.gz',
                    'statics': '<%= project.build.releaseDirectory %>/layout-statics.tar.gz',
                    'assets': '<%= project.build.releaseDirectory %>/layout-assets.tar.gz'
                }
            }
        },
        flags: {
            limited: {
                countries: ['de', 'at', 'be', 'bg', 'cy', 'hr', 'dk', 'es', 'ee', 'fi', 'fr', { 'gb': 'en' }, 'gr', 'hu', 'ie', 'it', 'lv', 'lt', 'lu', 'mt', 'nl', 'pl', 'pt', 'cz', 'ro', 'sk', 'si', 'se', 'ru'],
                // countries: ['fr', 'gb', 'es', 'bg', 'cz', 'ad', 'de', 'et', '--et--', '--ga--', 'hr', 'it', 'lv', 'lt', 'hu', 'mt', 'nl', 'pl', 'pt', 'ro', 'sk', '--sl--', 'fi', '--sv--', '--tr--', 'ru'],
                // countries: ['fr', 'en', 'es', 'bg', 'cs', 'da', 'de', 'et', 'el', 'ga', 'hr', 'it', 'lv', 'lt', 'hu', 'mt', 'nl', 'pl', 'pt', 'ro', 'sk', 'sl', 'fi', 'sv', 'tr', 'ru'],
                // regions: ['Europe'],
                target: '_tmp/flags',
                less: {
                    dest: 'flags.less',
                    class: 'flag'
                },
                sprite: {
                    sm: {
                        height: 18,
                        width: 20
                    },
                    md: {
                        height: 32,
                        width: 32,
                        html: 'i18n.html'
                    },
                    lg: {
                        height: 64,
                        width: 64
                    }
                }
            }
        }
    });

    require('load-grunt-tasks')(grunt);

    grunt.registerTask('buildAndPreserve', ['clean:tmp', 'requirejs', 'flags', 'less', 'extract', 'copy', 'cssmin', 'template']);
    grunt.registerTask('build', ['buildAndPreserve', 'clean:tmp']);
    grunt.registerTask('serve', ['buildAndPreserve', 'connect', 'watch']);
    grunt.registerTask('default', ['build']);

    grunt.registerTask('prepare-publish', ['prepublish']);
    grunt.registerTask('package', ['prepublish', 'targz']);

};