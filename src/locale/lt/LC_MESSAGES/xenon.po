# Guichet Entreprises
# Copyright (C) 2018
# This file is distributed under the same license as the Guichet Entreprises package.
# Florent Tournois <florent.tournois@finances.gouv.fr>, 2018.
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Gcuihet Entreprises\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-09-23 16:13+0200\n"
"PO-Revision-Date: 2018-05-08 10:04+0100\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: lt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.0.7\n"
"X-Poedit-Basepath: ../python\n"
"X-Poedit-SearchPath-0: .\n"
"X-Poedit-SourceCharset: UTF-8\n"

msgid "Drapeau"
msgstr "lt"

#. TRANSLATORS: Langue de traduction
#: process_layout.py:37
#: C:\dev\projet-ge.fr\win-tools\xenon2\python\xenon\constant.py:53
#: C:\dev\projet-ge.fr\win-tools\xenon2\python\xenon\constant.py:57
#: C:\dev\projet-ge.fr\xenon2\python\xenon\constant.py:57
#: C:\projet-ge\dev\xenon2\python\xenon\constant.py:61
#: generation_params.yml:240 generation_gp.yml:271
#: generation_parameters.yml:257
msgid "Français"
msgstr "Lietuvių"

#. TRANSLATORS: Baseline for Guichet Entreprises
#. TRANSLATORS: Base line pour Guichet Entreprises
#: process_layout.py:39
#: C:\dev\projet-ge.fr\win-tools\xenon2\python\process_layout.py:77
#: C:\dev\projet-ge.fr\win-tools\xenon2\python\process_layout.py:86
#: C:\dev\projet-ge.fr\win-tools\xenon2\python\process_layout.py:89
#: C:\dev\projet-ge.fr\xenon2\python\process_layout.py:89
#: C:\projet-ge\dev\xenon2\python\process_layout.py:44
msgid "Une seule adresse pour créer son entreprise"
msgstr "Vienas adresas verslui pradėti"

#. TRANSLATORS: Baseline for Guichet qualification
#. TRANSLATORS: Base line pour Guichet Qualifications
#: process_layout.py:41
#: C:\dev\projet-ge.fr\win-tools\xenon2\python\process_layout.py:79
#: C:\dev\projet-ge.fr\win-tools\xenon2\python\process_layout.py:88
#: C:\dev\projet-ge.fr\win-tools\xenon2\python\process_layout.py:91
#: C:\dev\projet-ge.fr\xenon2\python\process_layout.py:91
#: C:\projet-ge\dev\xenon2\python\process_layout.py:51
msgid ""
"Une seule adresse pour la reconnaissance de vos qualifications "
"professionnelles"
msgstr "Vienas profesinės kvalifikacijos pripažinimo adresas"

#. TRANSLATORS: Baseline for Guichet Partenaires
#. TRANSLATORS: Base line pour Guichet Partenaires
#: process_layout.py:43
#: C:\dev\projet-ge.fr\win-tools\xenon2\python\process_layout.py:81
#: C:\dev\projet-ge.fr\win-tools\xenon2\python\process_layout.py:91
#: C:\dev\projet-ge.fr\win-tools\xenon2\python\process_layout.py:94
#: C:\dev\projet-ge.fr\xenon2\python\process_layout.py:94
#: C:\projet-ge\dev\xenon2\python\process_layout.py:59
msgid "Une seule adresse pour les partenaires du service Guichet Entreprises"
msgstr "Vienas adresas \"Business Service\" klientų partneriams"

#: C:\dev\projet-ge.fr\win-tools\xenon2\python\xenon\constant.py:57
#: C:\dev\projet-ge.fr\win-tools\xenon2\python\xenon\constant.py:65
#: C:\dev\projet-ge.fr\win-tools\xenon2\python\xenon\constant.py:61
#: C:\dev\projet-ge.fr\win-tools\xenon2\python\xenon\constant.py:69
#: C:\dev\projet-ge.fr\xenon2\python\xenon\constant.py:61
#: C:\projet-ge\dev\xenon2\python\xenon\constant.py:65
#: generation_params.yml:243 generation_gp.yml:274
#: generation_parameters.yml:260
msgid "Compte"
msgstr "sąskaita"

#: C:\dev\projet-ge.fr\win-tools\xenon2\python\xenon\constant.py:59
#: C:\dev\projet-ge.fr\win-tools\xenon2\python\xenon\constant.py:63
#: C:\dev\projet-ge.fr\xenon2\python\xenon\constant.py:63
#: C:\projet-ge\dev\xenon2\python\xenon\constant.py:67
#: generation_params.yml:245 generation_gp.yml:276
#: generation_parameters.yml:262
msgid "Inscription"
msgstr "registracija"

#: C:\dev\projet-ge.fr\win-tools\xenon2\python\xenon\constant.py:60
#: C:\dev\projet-ge.fr\win-tools\xenon2\python\xenon\constant.py:64
#: C:\dev\projet-ge.fr\win-tools\xenon2\python\xenon\constant.py:67
#: C:\dev\projet-ge.fr\xenon2\python\xenon\constant.py:67
#: C:\projet-ge\dev\xenon2\python\xenon\constant.py:71
#: generation_params.yml:248 generation_gp.yml:279
#: generation_parameters.yml:265
msgid "Connexion"
msgstr "Prisijungti"

#: C:\dev\projet-ge.fr\win-tools\xenon2\python\xenon\constant.py:61
#: C:\dev\projet-ge.fr\win-tools\xenon2\python\xenon\constant.py:69
#: C:\dev\projet-ge.fr\win-tools\xenon2\python\xenon\constant.py:65
#: C:\dev\projet-ge.fr\win-tools\xenon2\python\xenon\constant.py:73
#: C:\dev\projet-ge.fr\win-tools\xenon2\python\xenon\constant.py:79
#: C:\dev\projet-ge.fr\xenon2\python\xenon\constant.py:79
#: C:\projet-ge\dev\xenon2\python\xenon\constant.py:83
msgid "Aide"
msgstr "padėti"

#: C:\dev\projet-ge.fr\win-tools\xenon2\python\xenon\constant.py:67
#: C:\dev\projet-ge.fr\win-tools\xenon2\python\xenon\constant.py:71
#: C:\dev\projet-ge.fr\xenon2\python\xenon\constant.py:71
#: C:\projet-ge\dev\xenon2\python\xenon\constant.py:75
#: generation_params.yml:251 generation_gp.yml:282
#: generation_parameters.yml:268
msgid "Profil"
msgstr "profilis"

#: C:\dev\projet-ge.fr\win-tools\xenon2\python\xenon\constant.py:68
#: C:\dev\projet-ge.fr\win-tools\xenon2\python\xenon\constant.py:72
#: C:\dev\projet-ge.fr\win-tools\xenon2\python\xenon\constant.py:75
#: C:\dev\projet-ge.fr\xenon2\python\xenon\constant.py:75
#: C:\projet-ge\dev\xenon2\python\xenon\constant.py:79
#: generation_params.yml:254 generation_gp.yml:285
#: generation_parameters.yml:271
msgid "Déconnexion"
msgstr "Atsijungti"

#: C:\dev\projet-ge.fr\win-tools\xenon2\python\xenon\translate.py:43
msgid "kjhgkjhg"
msgstr "kjhgkjhg"

#. TRANSLATORS: Texte en haut de la page pour validation
#. TRANSLATORS: Texte de la barre en haut pour les cookies
#: C:\dev\projet-ge.fr\win-tools\xenon2\python\process_layout.py:93
#: C:\dev\projet-ge.fr\win-tools\xenon2\python\process_layout.py:94
#: C:\dev\projet-ge.fr\win-tools\xenon2\python\process_layout.py:97
#: C:\dev\projet-ge.fr\xenon2\python\process_layout.py:97
#: C:\projet-ge\dev\xenon2\python\process_layout.py:69 generation_gp.yml:250
msgid ""
"En poursuivant votre navigation sur ce site, vous acceptez l'utilisation de "
"Cookies permettant de réaliser des statistiques de visites. "
msgstr ""
"Vykdydami navigaciją šioje svetainėje, jūs sutinkate naudoti slapukus, "
"leidžiančius atlikti apsilankymų statistiką."

#. TRANSLATORS: lien vers le en savoir plus
#. TRANSLATORS: Texte de la barre en haut pour les cookies (ici c'est le lien)
#: C:\dev\projet-ge.fr\win-tools\xenon2\python\process_layout.py:96
#: C:\dev\projet-ge.fr\win-tools\xenon2\python\process_layout.py:98
#: C:\dev\projet-ge.fr\win-tools\xenon2\python\process_layout.py:99
#: C:\dev\projet-ge.fr\win-tools\xenon2\python\process_layout.py:102
#: C:\dev\projet-ge.fr\xenon2\python\process_layout.py:102
#: C:\projet-ge\dev\xenon2\python\process_layout.py:78 generation_gp.yml:253
msgid "En savoir plus"
msgstr "Sužinokite daugiau"

#. TRANSLATORS: Titre de la popup cookie
#: C:\dev\projet-ge.fr\win-tools\xenon2\python\process_layout.py:97
#: C:\dev\projet-ge.fr\win-tools\xenon2\python\process_layout.py:100
#: C:\dev\projet-ge.fr\win-tools\xenon2\python\process_layout.py:101
#: C:\dev\projet-ge.fr\win-tools\xenon2\python\xenon\constant.py:51
#: C:\dev\projet-ge.fr\xenon2\python\xenon\constant.py:51
#: C:\projet-ge\dev\xenon2\python\xenon\constant.py:51
#: generation_params.yml:219 generation_gp.yml:241
#: generation_parameters.yml:236
msgid "Title of the cookie page in markdown"
msgstr "Verslo langas"

#. TRANSLATORS: Texte de la popup cookie
#: C:\dev\projet-ge.fr\win-tools\xenon2\python\process_layout.py:98
#: C:\dev\projet-ge.fr\win-tools\xenon2\python\process_layout.py:102
#: C:\dev\projet-ge.fr\win-tools\xenon2\python\process_layout.py:103
#: C:\dev\projet-ge.fr\win-tools\xenon2\python\xenon\constant.py:53
#: C:\dev\projet-ge.fr\xenon2\python\xenon\constant.py:53
#: C:\projet-ge\dev\xenon2\python\xenon\constant.py:53
#: generation_params.yml:221 generation_gp.yml:243
#: generation_parameters.yml:238
msgid "Text of the cookie page in markdown"
msgstr ""
"Ši svetainė naudoja slapukus, kad pagerintų naudotojų patirtį ir pateiktų papildomų funkcijų. Šie duomenys nebus naudojami jus identifikuoti arba su jumis susisiekti.\n"
"\n"
"Spustelėję mygtuką ** sutinku **, jūs sutinkate su šia svetaine išsaugoti keletą nedidelių duomenų blokų savo kompiuteryje.\n"
"\n"
"Norėdami sužinoti daugiau apie slapukus ir vietinį saugojimą, apsilankykite [Commission Nationale de l'Informatique et des Libertés](https://www.cnil.fr/vos-obligations/sites-web-cookies-et-autres-traceurs/)."

#: C:\dev\projet-ge.fr\win-tools\xenon2\python\xenon\constant.py:80
#: C:\dev\projet-ge.fr\win-tools\xenon2\python\xenon\constant.py:84
#: C:\dev\projet-ge.fr\win-tools\xenon2\python\xenon\constant.py:91
#: C:\dev\projet-ge.fr\xenon2\python\xenon\constant.py:91
#: C:\projet-ge\dev\xenon2\python\xenon\constant.py:95
msgid "Site local"
msgstr "Vietinė svetainė"

#: C:\dev\projet-ge.fr\win-tools\xenon2\python\xenon\constant.py:81
#: C:\dev\projet-ge.fr\win-tools\xenon2\python\xenon\constant.py:85
#: C:\dev\projet-ge.fr\win-tools\xenon2\python\xenon\constant.py:92
#: C:\dev\projet-ge.fr\xenon2\python\xenon\constant.py:92
#: C:\projet-ge\dev\xenon2\python\xenon\constant.py:96
#: generation_params.yml:231 generation_gp.yml:262
#: generation_parameters.yml:248
msgid "Veuillez patienter pendant le chargement de la page."
msgstr "Palaukite, kol puslapis įkeltas."

#. TRANSLATORS: Titre de la popup cookie
#. --Machine translation--
#: C:\projet-ge\dev\xenon2\python\xenon\constant.py:55
#: generation_params.yml:223 generation_gp.yml:245
#: generation_parameters.yml:240
#, fuzzy
msgid "Title of the browser page in markdown"
msgstr "Banko Įmonės"

#. TRANSLATORS: Texte de la popup cookie
#. --Machine translation--
#: C:\projet-ge\dev\xenon2\python\xenon\constant.py:57
#: generation_params.yml:225 generation_gp.yml:247
#: generation_parameters.yml:242
#, fuzzy
msgid "Text of the browser page in markdown"
msgstr ""
"Geriau naršymo patirtį bankų Įmonės, rekomenduojame naudoti naršyklę [ \"Mozilla Firefox] (https://www.mozilla.org/fr/firefox/new/) arba [Google Chrome\"] ( \"https: // www.google.com/chrome/).\n"
"\n"
"Iš tiesų, kai kurios naršyklės gali nepalaikyti svetainės Enterprise Bank funkcionalumą."

#. TRANSLATORS: Texte de la barre en haut pour les cookies (ici c'est le
#. l'acceptation)
#. --Machine translation--
#: generation_gp.yml:256
msgid "J'accepte"
msgstr "Sutinku"
