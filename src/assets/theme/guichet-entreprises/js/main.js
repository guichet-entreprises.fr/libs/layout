define([], function () {

    return properties = {
        'code': 'ge',
        'title': 'Guichet Entreprises',
        'ribbon': 'G. Entreprises',
        'favicon': 'images/guichet-entreprises/favicon.png',
        'styles': 'css/theme.guichet-entreprises.min.css',
        'logo-mono': 'images/guichet-entreprises/logo/long-mono.png',
        'logo-base': 'images/guichet-entreprises/logo/long-base.png',
        'logo-baseline': 'GE/La vie c\'est comme une boite de chocolat ...',
        'footer': 'images/guichet-entreprises/logo/footer.png'
    };

});
