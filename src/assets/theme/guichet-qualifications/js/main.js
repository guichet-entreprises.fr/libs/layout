define([], function () {

    return properties = {
        'code': 'gq',
        'title': 'Guichet Qualifications',
        'ribbon': 'G. Qualifications',
        'favicon': 'images/guichet-qualifications/favicon.png',
        'styles': 'css/theme.guichet-qualifications.min.css',
        'logo-mono': 'images/guichet-qualifications/logo/long-mono.png',
        'logo-base': 'images/guichet-qualifications/logo/long-base.png',
        'logo-baseline': 'GQ/La vie c\'est comme une boite de chocolat ...',
        'footer': 'images/guichet-qualifications/logo/footer.png'
    };

});
