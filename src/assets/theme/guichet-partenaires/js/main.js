define([], function () {

    return properties = {
        'code': 'gp',
        'title': 'Guichet Partenaires',
        'ribbon': 'G. Partenaires',
        'favicon': 'images/guichet-partenaires/favicon.png',
        'styles': 'css/theme.guichet-partenaires.min.css',
        'logo-mono': 'images/guichet-partenaires/logo/long-mono.png',
        'logo-base': 'images/guichet-partenaires/logo/long-base.png',
        'logo-baseline': 'GP/La vie c\'est comme une boite de chocolat ...',
        'footer': 'images/guichet-partenaires/logo/footer.png'
    };

});
