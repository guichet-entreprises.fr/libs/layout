var themeOptions = {
    apply: <%= applyTheme %>,
    publicResourcesBaseUrl: '<%= basePath.replace(/\/$/, '') %>'
};
var baseUrl = /*[[@{/}]]*/ '/';
var locale = {
    language: 'fr',
    country: 'FR',
    full: 'fr_FR'
};
