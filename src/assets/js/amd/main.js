requirejs(['jquery', 'bootstrap', 'lib/cookie-bar', 'lib/authentication', 'jquery.counterup', 'lib/sections', 'lib/environment', 'lib/browser-alert', 'lib/font-size', 'lib/search'], function ($, _) {

    console.log('[main] loading main module', $.fn);

    $(function () {
        console.log('[main] initialization ...');

        $('body').on('click', '[data-toggle="popover"]', function (evt) { evt.preventDefault(); evt.stopPropagation(); }).popover();

        var path = (window.location.pathname || '');

        $('#global-navigation > ul > li.active').removeClass('active');
        $('#global-navigation a').each(function () {
            var lnk = $(this), href = lnk.attr('href');
            if (!href || path.substr(path.length - href.length) != href) {
                return;
            }

            lnk.closest('[role="presentation"]').addClass('active');
        });

        $('.number').counterUp({
            delay: 25,
            time: 1000
        });

        $('[data-background]').each(function () {
            var elm = $(this), url = elm.data('background');
            elm.css('background-image', 'url(' + url + ')');
        });

    });

    console.debug('[Main] page loaded');

});
