define(['jquery', 'lib/storage'], function ($, storage) {

    const EVT_NAME = 'click.fontSize';
    const STYLE_NAME = 'font-size';
    const STEPPING = {
        'em': 0.2,
        'px': 2
    };

    function init() {
        $('*[data-toggle="font-size"]').each(function () {
            var $this = $(this);
            var target = $($this.data('target'));
            var defaultFontSize = target.css(STYLE_NAME);
            var storedValue = storage('fontSize');

            if (storedValue) {
                target.css(STYLE_NAME, storedValue);
            }

            $this.off(EVT_NAME).on(EVT_NAME, '*[role="font-size-default"]', function (evt) {
                evt.stopPropagation();
                target.css(STYLE_NAME, defaultFontSize);
                storage('fontSize', defaultFontSize);
            }).on(EVT_NAME, '*[role="font-size-up"]', function (evt) {
                var m = /([0-9]+)\s*(px|em)/i.exec(target.css(STYLE_NAME));
                var newFontSize;

                evt.stopPropagation();

                if (m && STEPPING[m[2]]) {
                    newFontSize = '' + (parseFloat(m[1]) + STEPPING[m[2]]) + m[2];
                    target.css(STYLE_NAME, newFontSize);
                    storage('fontSize', newFontSize);
                }
            }).on(EVT_NAME, '*[role="font-size-down"]', function (evt) {
                var m = /([0-9]+)\s*(px|em)/i.exec(target.css(STYLE_NAME));
                var newFontSize;

                evt.stopPropagation();

                if (m && STEPPING[m[2]]) {
                    newFontSize = '' + (parseFloat(m[1]) - STEPPING[m[2]]) + m[2];
                    target.css(STYLE_NAME, newFontSize);
                    storage('fontSize', newFontSize);
                }
            });
        });
    }

    $(function () {
        init();
    });

    return init;

});
