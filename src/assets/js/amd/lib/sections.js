define(['jquery'], function ($) {

    function polyfill() {
        if (!Object.values) {
            Object.values = function (obj) {
                var lst = [];
                for (var k in obj) {
                    if (typeof k === 'string' && obj.propertyIsEnumerable(k)) {
                        lst.push(obj[k]);
                    }
                }
                return lst;
            };
        }
    }

    $(function () {
        var styles = {};

        if ($('body').width() >= 768) {
            $('section[data-offset]').each(function () {
                var section = $(this), offset = section.attr('data-offset');
                section.css('margin-top', offset);
            });
        }

        polyfill();
    });

});
