define([], function () {

    if (location.protocol === 'file:') {
        return function (name, value) {
            if (undefined === value) {
                return sessionStorage.getItem(name);
            } else if (null === value) {
                sessionStorage.removeItem(name);
            } else {
                sessionStorage.setItem(name, value);
            }
        };
    } else {
        return function (name, value) {
            if (undefined === value) {
                return document.cookie.replace(new RegExp('^(?:(?:|.*;\\\s*)' + name + '\\\s*=\\\s*([^;]*).*$)|^.*$'), '$1');
            } else if (null === value) {
                document.cookie = name + '=; expires=0';
            } else {
                var dlc = new Date();
                dlc.setFullYear(dlc.getFullYear() + 1);
                document.cookie = name + '=' + value + '; expires=' + new Date(dlc);
            }
        };
    }

});
