define(['jquery'], function ($) {

    function init() {
        $('#globalSearch').on('submit', function (evt) {
            var $this = $(this);
            var q = $('input[name="q"]', $this).val();

            evt.preventDefault();

            location.href = $this.attr('action') + '?q=' + encodeURI(q + ' site:' + location.hostname);
        });
    }

    $(init);

});
