define([], function () {

    var PTN_ALL_EXCEPT_IE = /(Firefox|Seamonkey|Chrome|Chromium|Safari|OPR|Opera|Edge)\/([0-9.]+)/g ;
    var PTN_IE = /(MSIE) ([0-9.]+)/ ;

    var browsers;

    function detect() {
        if (browsers) {
            return browsers;
        }

        var lst = {};
        var m = PTN_IE.exec(navigator.userAgent);
        if (m) {
            lst['Internet Explorer'] = m[2];
        } else {
            while (m = PTN_ALL_EXCEPT_IE.exec(navigator.userAgent)) {
                lst[m[1]] = m[2];
            }
        }

        return browsers = lst;
    }

    return {
        isFirefox: function () {
            var browsers = detect();
            return browsers['Firefox'] && !browsers['Seamonkey'];
        },
        isSeamonkey: function () {
            var browsers = detect();
            return undefined !== browsers['Seamonkey'];
        },
        isChrome: function () {
            var browsers = detect();
            return browsers['Chrome'] && !browsers['Chromium'];
        },
        isSafari: function () {
            var browsers = detect();
            return browsers['Safari'] && !browsers['Chrome'] && !browsers['Chromium'];
        },
        isOpera: function () {
            var browsers = detect();
            return undefined !== browsers['Opera'] || undefined !== browsers['OPR'];
        },
        isInternetExplorer: function () {
            var browsers = detect();
            return undefined !== browsers['Internet Explorer'];
        },
        isEdge: function () {
            return !this.isInternetExplorer() && window.styleMedia;
        },
        which: function () {
            for (var k in this) {
                if (k.substr(0, 2) === 'is' && typeof this[k] === 'function' && this[k]()) {
                    return k.substr(2);
                }
            }
        }
    }

});
