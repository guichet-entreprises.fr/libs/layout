define(['jquery', 'lib/storage'], function ($, storage) {

    var tokenPattern = /^([^=]+)=(.*)$/;

    function updateAccountMenu() {
        var user = registerUserAuthentication();

        if (user) {
            $('[role="account"] > a > [role="label"]').text([user.forName, user.name].filter(function (elm) { return null != elm; }).map(function (elm) { return elm.charAt(0).toUpperCase() + elm.slice(1); }).join(' '));
        }
    }

    function registerUserAuthentication() {
        var user;
        var tokens = storage('sessionidge');
        if (tokens) {
            tokens.split('&').forEach(function (elm) {
                var m = tokenPattern.exec(elm);
                if (m && 'X-GE-ACCOUNT' === m[1]) {
                    user = JSON.parse(atob(m[2]));
                }
            });
        }

        var connectedCookieValue = storage('_geconnected');
        if ('connected' == connectedCookieValue || user) {
            $('body').addClass('user-authenticated');
        } else {
            $('body').removeClass('user-authenticated');
        }

        return user;
    }

    $(function () {
        updateAccountMenu();

        if (location.protocol === 'file:') {
            $('[role="account"] [role="login"]').on('click', function (evt) {
                evt.preventDefault();
                storage('_geconnected', 'connected');
                history.go();
            });

            $('[role="account"] [role="logout"]').on('click', function (evt) {
                evt.preventDefault();
                storage('_geconnected', null);
                history.go();
            });
        }
    });

});
