define(['jquery'], function ($) {

    const HOST_PATTERN = /([^.]+)(?:\.((?:([^.]+)\.)?([^.]+\.[^.]+)))/;
    const LINK_PATTERN = /^([^:]+):\/\/([^.]+)(?:\.([^.]+))?\.([^.]+\.[^./]+)/;
    const RIBBON = {
        'dev': 'Developpement',
        'qual': 'Qualification',
        'int': 'Integration',
        'recr': 'Recette R.',
        'recj': 'Recette J.'
    };

    function init() {
        var environment, domain;
        if (location.protocol !== 'file:') {
            var m = HOST_PATTERN.exec(location.hostname);
            if (m) {
                domain = m[2];
                environment = m[3];
            }
        }

        if (domain) {
            $('a[data-toggle="domain"]').each(function () {
                var self = $(this), href = self.attr('href');
                self.attr('href', href.replace(LINK_PATTERN, '$1://$2.' + domain));
            });
        }

        if (environment) {
            $('a[data-toggle="env"]').each(function () {
                var self = $(this), href = self.attr('href');
                self.attr('href', href.replace(LINK_PATTERN, '$1://$2.' + environment + '.$4'));
            });

            $('<span>', {
                'id': 'ribbon',
                'class': 'ribbon'
            }).html(RIBBON[environment] || 'Unknown').prependTo('body > div.page-wrap');
        }
    }

    $(function () {
        init();
    });

});