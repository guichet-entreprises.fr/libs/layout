define([ 'jquery', 'lib/browsers' ], function ($, Browser) {

    $(function () {
        if (!Browser.isFirefox() && !Browser.isChrome()) {
            console.log('[BrowserAlert] Bad browser ...');
            $('#dlgBrowserAlert').modal('show');
        }
    });

});
