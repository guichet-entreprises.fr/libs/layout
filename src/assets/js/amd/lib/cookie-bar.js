define([ 'jquery', 'lib/storage' ], function ($, storage) {

    function init() {
        var storedCookieBar = storage('cb-enabled');
        console.debug('[CookieBar] value =', storedCookieBar);
        if (!storedCookieBar) {
            // var cookieBar = $('<div id="cookieBar" class="container"><div lang="es">Utilizamos cookies propias para recoger datos estadísticos. Si continúa navegando, está aceptando su uso. Puede obtener más información o cambiar la configuración en política de cookies. <a class="btn" href="#" role="moreInformations">Más información</a>. <a class="btn btn-primary" href="#" role="acceptCookies">Aceptar</a>.</div></div>') //
            // var cookieBar = $('<div id="cookieBar" class="container"><div lang="en">If you continue browsing this website, you consent to our use of cookies. Cookies help us perform statistics on the number of visits on our websites. <a class="btn" href="#" role="moreInformations">More information</a>. <a class="btn btn-primary" href="#" role="acceptCookies">I agree</a>.</div></div>') //
            // var wrapper = $('<div id="cookieBarWrapper"></div>').prependTo('body > .page-wrap');
            // var cookieBar = $('<div id="cookieBar" class="container"><div>En poursuivant votre navigation sur ce site, vous acceptez l\'utilisation de Cookies permettant de réaliser des statistiques de visites. <a href="#" role="moreInformations" data-toggle="modal" data-target="#dlgCookiesAbout">En savoir plus</a> <a class="btn btn-primary" href="#" role="acceptCookies">J\'accepte</a></div></div>') //
            //     .prependTo(wrapper);

            var wrapper = $('#cookieBarWrapper');
            $('#cookieBar').on('click', 'a[role="acceptCookies"]', function (evt) {
                evt.preventDefault();
                storage('cb-enabled', 'accepted');
                wrapper.hide('blind');
            });

            wrapper.show();
        }
    }

    $(function () {
        init();
    });

    return init;

});
