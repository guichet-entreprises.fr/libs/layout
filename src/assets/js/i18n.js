(function () {

    const lngs = ['fr', 'en', 'es', 'de', 'it', 'pt', 'ru'];
    const defaultLng = 'fr';

    function findLanguage() {
        let m = /^\/([-a-z]{2})(\/.*)$/.exec(location.pathname);
        return m ? [m[1], m[2]] : [null, location.pathname];
    }

    function redirect() {
        let [lng, path] = findLanguage();

        console.debug('[i18n] language detected :', lng, ', path : ', path);

        if (!lng || lngs.indexOf(lng) < 0) {
            if (navigator.language && lngs.indexOf(navigator.language) >= 0) {
                lng = navigator.language;
            } else {
                lng = defaultLng;
            }

            location.pathname = `/${lng}${path}`;
        }
    }

    redirect();

})();
