'use strict'

module.exports = function (grunt) {

    const async = require('async');
    const path = require('path');
    const utils = require('../../../build/utils');

    grunt.registerMultiTask('prepublish', 'Prepare a subdirectory for publishing', function () {

        let opts = this.options({});
        let done = this.async();
        let targetDir = this.data.target;

        if (targetDir) {
            grunt.file.mkdir(targetDir);
        }

        async.eachSeries(this.files, function (elm, next) {
            if (elm.src.length != 1) {
                grunt.log.error('Need one and only source element');
                return;
            }

            let rootDir = elm.src[0];
            if (!grunt.file.exists(rootDir)) {
                grunt.log.error('Source element has to be a directory');
                return;
            }

            let nfo = utils.extend(opts.pkg, {
                name: opts.pkg.name.replace(/\{name\}/, elm.dest.replace(/^.*\/([^\/]+)/, '$1'))
            });

            if (targetDir) {
                grunt.file.write(path.join(targetDir, 'package-' + elm.dest + '.json'), JSON.stringify(nfo, null, 4));
            } else {
                grunt.file.write(path.join(rootDir, 'package.json'), JSON.stringify(nfo, null, 4));
            }

            next();
        }, done);
    });

};
