'use strict';

module.exports = function (grunt) {

    const async = require('async');
    const mvn = require('maven-deploy');
    const utils = require('../../../build/utils');

    grunt.registerMultiTask('maven', 'Deploy to maven repository', function () {

        let opts = this.options({});
        let done = this.async();

        async.eachSeries(this.files, function (elm, next) {
            if (!elm.src || !elm.src[0]) {
                return next();
            }

            let localOpts = utils.extend(opts, {
                classifier: elm.dest
            });

            grunt.log.debug('Deploy', elm.src[0]);

            mvn.config(localOpts);
            mvn.deploy('scnge', elm.src[0], function (err) {
                if (err) {
                    grunt.log.error(err);
                }
                next();
            });
        }, done);
    });

};
