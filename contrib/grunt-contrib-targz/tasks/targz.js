'use strict';

module.exports = function (grunt) {

    let fs = require('fs');
    let zlib = require('zlib');
    let tar = require('tar-stream');

    grunt.registerMultiTask('targz', 'Build tar.gz package', function () {
        let pack = tar.pack();

        let done = this.async();

        let statNext = (function (files) {
            let queue = files;
            return function loop(callback) {
                let nextFile = queue.shift();
                if (!nextFile) return callback();

                fs.stat(nextFile.src[0], function (err, stat) {
                    if (err) return callback(err);

                    callback(null, nextFile, stat);
                });
            };
        })(this.files);

        let onStat = function (err, file, stat) {
            if (err) {
                grunt.log.error('Stat  error : ' + err);
                return pack.destroy();
            }
            if (!file) return pack.finalize();

            if (stat.isDirectory()) {
                return onNextEntry();
            }

            let header = {
                name: file.dest,
                mtime: stat.mtime,
                size: stat.size,
                type: 'file',
                uid: stat.uid,
                gid: stat.gid
            };

            let entry = pack.entry(header, onNextEntry);
            let rs = fs.createReadStream(file.src[0]);
            rs.on('error', function (err) {
                grunt.log.error('Reading ' + file.src[0] + ' error : ' + err);
                entry.destroy(err);
            });

            rs.pipe(entry);
        };

        let onNextEntry = function (err) {
            if (err) {
                grunt.log.error('Entry error : ' + err);
                return pack.destroy(err);
            }
            statNext(onStat);
        }

        onNextEntry();

        pack.pipe(zlib.createGzip()).pipe(fs.createWriteStream(this.data.target)).on('finish', function () {
            done();
        });
    });

};
