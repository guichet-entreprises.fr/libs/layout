'use strict';

module.exports = function (grunt) {

    const fs = require('fs');
    const path = require('path');
    const async = require('async');
    const util = require('util');
    const sharp = require('sharp');
    const Spritesmith = require('spritesmith');
    const countries = require('world-countries/dist/countries.json');
    const isoCountries = require('i18n-iso-countries');


    const countriesByKey = {};
    const countriesByRegion = {};


    countries.forEach(country => {
        countriesByKey[country.name.common.toLowerCase()] = country;
        countriesByKey[country.name.official.toLowerCase()] = country;
        countriesByKey[country.cca2.toLowerCase()] = country;

        if (country.region) {
            let region = countriesByRegion[country.region.toLowerCase()] || [];
            region.push(country);
            countriesByRegion[country.region.toLowerCase()] = region;
        }
    });



    grunt.registerMultiTask('flags', 'Build country flags sprite and stylesheet', function () {

        let done = this.async();

        let opts = this.options({});
        let target = this.data.target;

        let languageByCountryCode = {};
        let countryByFilename = {};
        function addCountry(country) {
            if (country) {
                let filename = util.format('node_modules/world-countries/data/%s.svg', country.cca3.toLowerCase());
                countryByFilename[filename] = country;
            }
        }

        if (this.data.regions) {
            this.data.regions.forEach(region => {
                (countriesByRegion[region.toLowerCase()] || []).forEach(addCountry);
            });
        }

        if (this.data.countries) {
            this.data.countries.forEach(elm => {
                let key = typeof elm == 'string' ? elm : Object.keys(elm)[0];
                let country = countriesByKey[key.toLowerCase()];
                if ('string' == typeof elm) {
                    languageByCountryCode[key] = [ country.cca2 ];
                } else {
                    languageByCountryCode[key] = [ country.cca2, elm[key] ]
                }
                languageByCountryCode[key] = languageByCountryCode[key].map((str) => str.toLowerCase());
                // console.log(`${key} (${languageByCountryCode[key]}) => ${country ? country.name.common : '<unknown>'}`);
                addCountry(country);
            });
        }

        let workingDir = fs.mkdtempSync('tmp-');

        function resize(name, opts) {
            return new Promise((resolve, reject) => {
                let sprites = [];
                const namedWorkingDir = path.resolve(workingDir, name);
                fs.mkdirSync(namedWorkingDir, { recursive: true });

                let lst = Object.keys(countryByFilename).map(src => {
                    const nfo = countryByFilename[src];
                    // const langAlpha3 = Object.keys(nfo.languages)[0];
                    // const lang = isoCountries.alpha3ToAlpha2(langAlpha3.toUpperCase());
                    // console.log(`${nfo.cca2} : ${langAlpha3} => ${lang}`, Object.keys(nfo.languages));
                    const dst = path.resolve(namedWorkingDir, nfo.cca2.toLowerCase() + '.png');
                    sprites.push(dst);
                    return sharp(src) //
                        .resize({
                            width: opts.width,
                            height: opts.height,
                            fit: sharp.fit.contain,
                            position: sharp.strategy.center,
                            background: { r: 0, g: 0, b: 0, alpha: 0 }
                        }) //
                        .png()
                        .toFile(dst);
                });

                Promise.all(lst) //
                    .then(data => resolve(sprites)) //
                    .catch(err => reject(err));
            });
        }

        const stylesheetOpts = this.data.less;

        Promise.all(Object.keys(this.data.sprite).map(spriteName => {
            const spriteFileName = `flags-${spriteName}.png`;
            const spriteOpts = this.data.sprite[spriteName];

            return new Promise((resolve, reject) => {
                resize(spriteName, spriteOpts) //
                    .then(sprites => {
                        Spritesmith.run({ src: sprites }, (err, result) => {
                            fs.mkdirSync(target, { recursive: true });
                            fs.writeFileSync(path.resolve(target, spriteFileName), result.image);
                            let stylesheet = Object.keys(result.coordinates).map(filename => {
                                let nfo = result.coordinates[filename];
                                let cca2 = path.basename(filename).replace(/^([^.]+)\.png$/, '$1');
                                let lng = languageByCountryCode[cca2];
                                let left = nfo.x > 0 ? -nfo.x : 0;
                                let top = nfo.y > 0 ? -nfo.y : 0;
                                return languageByCountryCode[cca2].map((lng) => `    &.${lng} { background-position: ${left}px ${top}px; }`).join('\n');
                                // return `    &.${lng} { background-position: ${left}px ${top}px; }`;
                            });

                            stylesheet = [
                                `    background-image: url(\'../images/${spriteFileName}\');`,
                                `    height: ${spriteOpts.height}px;`,
                                `    width: ${spriteOpts.width}px;`,
                                '',
                                ...stylesheet
                            ]

                            if (spriteOpts.html) {
                                let html = '<div class="row">\n' + //
                                    Object.keys(result.coordinates).map(filename => {
                                        let cca2 = path.basename(filename).replace(/^([^.]+)\.png$/, '$1');
                                        let lng = languageByCountryCode[cca2][languageByCountryCode[cca2].length - 1];
                                        let country = countriesByKey[cca2];
                                        return `    <div class="col-md-4"><a class="${stylesheetOpts.class}-${spriteName}" href="#${lng}"><span class="${stylesheetOpts.class} ${stylesheetOpts.class}-${spriteName} ${lng}"></span> ${country.name.common} (${lng.toUpperCase()})</a></div>`;
                                    }).join('\n') + //
                                    '\n</div>\n';
                                fs.writeFileSync(path.resolve(target, spriteOpts.html), html);
                            }

                            resolve({
                                name: spriteName,
                                stylesheet: stylesheet.join('\n'),
                                width: spriteOpts.width,
                                height: spriteOpts.height
                            });
                        });
                    }) //
                    .catch(reject);
            });
        })) //
            .then(data => {
                fs.writeFileSync( //
                    path.resolve(target, stylesheetOpts.dest), //
                    `span.${stylesheetOpts.class} {\n` + //
                    '    background-position: 0 0;\n' + //
                    '    background-repeat: no-repeat;\n' + //
                    '    display: inline-block;\n' + //
                    '    margin-right: 6px;\n\n' + //
                    '    vertical-align: middle;\n\n' + //
                    data.slice(0, 1).map(elm => `    &.${stylesheetOpts.class} {\n${elm.stylesheet.replace(/^/gm, '    ')}\n    }\n`)[0] + //
                    data.map(elm => `    &.${stylesheetOpts.class}-${elm.name} {\n${elm.stylesheet.replace(/^/gm, '    ')}\n    }\n`).join('\n') + //
                    '}\n' + //
                    data.map(elm => `a.${stylesheetOpts.class}-${elm.name} { line-height: ${elm.height}px; }`).join('\n') //
                );
                fs.rmdirSync(workingDir, { recursive: true });
                done();
            });

    });

};
