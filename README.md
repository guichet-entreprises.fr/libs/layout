# Layout

Layout template for all our applications.

## Working with static base layout

To build static base layout, without scripts, styles and other resources for each domain, execute command line :
```
npm install
```

To start local server, execute command line :
```
npm start
```

