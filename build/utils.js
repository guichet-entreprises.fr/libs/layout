'use strict';

const path = require('path');



/**
 * Build CSS file item.
 * 
 * @param basePath source base path
 * @param name resource name
 * @returns file item
 */
exports.buildStyle = function (basePath, name) {
    return {
        src: [basePath + '/' + name],
        dest: '_tmp/assets/css/libs/' + path.basename(basePath) + '/' + path.basename(name).replace(/\.[^.]+$/, '.css')
    }
};


/**
 * Merge all argument objects into a new one and return it.
 * 
 * @returns merged object
 */
exports.extend = function () {
    var sources = Array.from(arguments);
    var dst = {}, src;

    if (1 === sources.length && Array.isArray(sources[0])) {
        sources = sources[0];
    }

    while (src = sources.shift()) {
        Object.keys(src).forEach(function (key) {
            dst[key] = src[key];
        });
    }
    return dst;
};


const entities = {
    'quot': '"',
    'lt': '<',
    'gt': '>'
};

/**
 * Decode HTML
 * 
 * @returns decoded HTML
 */
exports.htmlDecode = function (src) {
    return src.replace(/\&([a-z]+);/gi, function (src, entity) {
        return entities[entity] || '&' + entity + ';';
    });
};


