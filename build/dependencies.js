'use strict';

const utils = require('./utils');


class DependencyLoader {

    static build(grunt) {
        const pkg = grunt.file.readJSON('package.json');
        const overrides = grunt.file.readJSON('build/overrides.json');

        /*
         * Build RequireJS external dependencies
         */
        var requireJsEncapsulation = {};
        var vendorsDependencies = Object.keys(pkg.dependencies || {}) //
            .filter(elm => elm !== 'requirejs') //
            .filter(elm => elm !== 'jquery-ui') //
            .reduce(function (dst, dep) {
                var path = '';
                var nfo = utils.extend(require('../node_modules/' + dep + '/package.json'), overrides[dep] || {});
                if (nfo.main) {
                    if (typeof nfo.main === 'string') {
                        path = nfo.main;
                    } else if (nfo.main.find) {
                        path = nfo.main.find(elm => elm.match(/\.js$/));
                    } else {
                        return dst;
                    }
                    dst[dep] = 'node_modules/' + dep + path.replace(/^\.?[\/]*/, '/').replace(/\.js$/, '');
                }
                if (nfo.requirejs) {
                    requireJsEncapsulation[dep] = nfo.requirejs;
                }
                return dst;
            }, {});

        grunt.file.expand({ cwd: 'node_modules/jquery-ui/ui' }, ['**/*.js']) //
            .filter(elm => elm !== 'core.js') //
            .forEach(function (dep) {
                vendorsDependencies['jquery-ui/' + dep.replace(/\.js$/, '')] = 'node_modules/jquery-ui/ui/' + dep.replace(/\.js$/, '');
            });


        let obj = new DependencyLoader();
        obj.vendors = vendorsDependencies;
        obj.needEncapsulation = requireJsEncapsulation;
        obj.empties = Object.keys(vendorsDependencies || {}).reduce(function (dst, key) {
            dst[key] = 'empty:';
            return dst;
        }, {});

        return obj;
    }



    /**
     * @param moduleName
     * @param contents
     * @returns
     */
    encapsulate(moduleName, contents) {
        var nfo = this.needEncapsulation[moduleName], deps = [], params = [];

        if (undefined === nfo) {
            return contents;
        }

        Object.keys(nfo.deps).forEach(function (depName) {
            if (nfo.deps[depName]) {
                deps.unshift(depName);
                params.unshift(nfo.deps[depName]);
            } else {
                deps.push(depName);
            }
        });
        return "define('" + moduleName + "', [ " + deps.map(depName => "'" + depName + "'").join(', ') + " ], function (" + params.join(', ') + ") {\n" + contents + "\n});";
    }

}


module.exports = DependencyLoader;
